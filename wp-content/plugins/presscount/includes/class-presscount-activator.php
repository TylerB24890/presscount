<?php

/**
 * Fired during plugin activation
 *
 * @link       http://elexicon.com
 * @since      0.5.2
 *
 * @package    PressCount
 * @subpackage presscount/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      0.5.2
 * @package    PressCount
 * @subpackage presscount/includes
 * @author     Tyler Bailey <tylerb.media@gmail.com>
 */
class PressCount_Activator {

	public static function activate() {
		return;
	}
}
