# PressCount #

A Wordpress plugin to track shares, likes, tweets, pins, and +1s across multiple social networks. A simple GUI is provided in the Wordpress Dashboard to view your post statistics.

### What is this repository for? ###

* Analytic junkies who are interested in seeing the popularity of their posts on social networks

### How do I get set up? ###

* Simply upload the 'presscount' folder in your plugins directory and activate it through the wp-admin dashboard.
* Add the shortcode `[share_count]` to display the individual number of all the social networks combined. 
* If you wish to add ' Shares' to the end of the returned share count add the `text=true` parameter to the shortcode for a final shortcode of `[share_count text=true]`. See the 'Overview' page for more information & screenshots. 

### Who do I talk to? ###

* Tyler Bailey <tylerb.media@gmail.com>